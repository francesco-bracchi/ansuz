(defproject ansuz "0.1.1"
  :description "Ansuz is a language to create parsers for clojure"
  :plugins [[lein-marginalia "0.7.1"]]
  :dependencies [[org.clojure/clojure "1.4.0"]])

